# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

ARG SECRET_NUMBER_ARG
RUN echo ${SECRET_NUMBER_ARG}

ENV SECRET_NUMBER=$SECRET_NUMBER_ARG
RUN echo ${SECRET_NUMBER}

CMD [ "set", "FLASK_ENV=app" ]
CMD [ "gunicorn", "--pid", "/tmp/service.pid", "--workers", "1", "--threads", "1", "-b", "0.0.0.0:7000", "app:app"]
